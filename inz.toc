\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Cel i zakres pracy}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Wprowadzenie}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}Analogowy Interfejs}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Zasada dzia\IeC {\l }ania}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Schemat elektroniczny}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Wygl\IeC {\k a}d PCB}{9}{section.2.3}
\contentsline {chapter}{\numberline {3}Przetwornik analogowo cyfrowy}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Zasada dzia\IeC {\l }ania}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Schemat elektroniczny}{12}{section.3.2}
\contentsline {section}{\numberline {3.3}Wygl\IeC {\k a}d PCB}{14}{section.3.3}
\contentsline {chapter}{\numberline {4}Bezpo\IeC {\'s}rednio programowalna macierz bramek}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Zasada dzia\IeC {\l }ania}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}Schemat elektroniczny}{17}{section.4.2}
\contentsline {section}{\numberline {4.3}Konfiguracja FPGA}{19}{section.4.3}
\contentsline {section}{\numberline {4.4}Wygl\IeC {\k a}d PCB}{24}{section.4.4}
\contentsline {chapter}{\numberline {5}P\IeC {\l }ytka steruj\IeC {\k a}ca}{25}{chapter.5}
\contentsline {section}{\numberline {5.1}Zasada dzia\IeC {\l }ania}{25}{section.5.1}
\contentsline {section}{\numberline {5.2}Schemat elektroniczny}{27}{section.5.2}
\contentsline {section}{\numberline {5.3}Inicjalizacja sprz\IeC {\k e}tu}{29}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Wewn\IeC {\k e}trzne zegary mikrokontrolera}{29}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}SDRAM}{29}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}LTDC}{31}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Karta SD}{32}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}System przerwa\IeC {\'n} zewn\IeC {\k e}trznych}{33}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}Konwerter cyfrowo analogowy}{34}{subsection.5.3.6}
\contentsline {subsection}{\numberline {5.3.7}Programowalne wzmacniacze operacyjne}{35}{subsection.5.3.7}
\contentsline {subsection}{\numberline {5.3.8}Programowalny generator zegara}{36}{subsection.5.3.8}
\contentsline {subsection}{\numberline {5.3.9}Przetwornik analogowo-cyforwy}{36}{subsection.5.3.9}
\contentsline {subsection}{\numberline {5.3.10}Panel dotykowy}{37}{subsection.5.3.10}
\contentsline {section}{\numberline {5.4}Wygl\IeC {\k a}d PCB}{39}{section.5.4}
\contentsline {chapter}{\numberline {6}Panel zewn\IeC {\k e}trzny}{41}{chapter.6}
\contentsline {section}{\numberline {6.1}Zasada dzia\IeC {\l }ania}{41}{section.6.1}
\contentsline {section}{\numberline {6.2}Schemat elektroniczny}{42}{section.6.2}
\contentsline {section}{\numberline {6.3}Wygl\IeC {\k a}d PCB}{44}{section.6.3}
\contentsline {chapter}{\numberline {7}Zasilanie}{45}{chapter.7}
\contentsline {section}{\numberline {7.1}Schemat elektroniczny}{45}{section.7.1}
\contentsline {section}{\numberline {7.2}Wygl\IeC {\k a}d PCB}{46}{section.7.2}
\contentsline {chapter}{\numberline {8}System akwizycji i przedstawiania danych pomiarowych}{47}{chapter.8}
\contentsline {section}{\numberline {8.1}Zasada dzia\IeC {\l }ania maszyny stan\IeC {\'o}w}{48}{section.8.1}
\contentsline {section}{\numberline {8.2}Cz\IeC {\k e}\IeC {\'s}ci sk\IeC {\l }adowe systemu akwizycji}{50}{section.8.2}
\contentsline {section}{\numberline {8.3}Drogi rozwoju}{51}{section.8.3}
\contentsline {chapter}{\numberline {9}Podsumowanie}{53}{chapter.9}
\contentsline {chapter}{Bibliografia}{53}{chapter.9}
